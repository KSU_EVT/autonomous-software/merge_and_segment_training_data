#!/usr/bin/env python3

import sys
import os

def renumber_classes(file_path, class_mapping):
    """
    Renumber the classes in the YOLO txt label file based on the given mapping.
    """
    with open(file_path, 'r') as f:
        lines = f.readlines()

    # Create a new list of lines with the renumbered classes
    new_lines = []
    for line in lines:
        parts = line.split()
        class_id = int(parts[0])

        # Check if the class_id needs to be renumbered
        if class_id in class_mapping:
            parts[0] = str(class_mapping[class_id])
            new_lines.append(' '.join(parts) + '\n')

    # Write the renumbered lines back to the file
    with open(file_path, 'w') as f:
        f.writelines(new_lines)

def main():
    if len(sys.argv) < 2:
        print(f"Usage: {sys.argv[0]} <class1> <class2> ...")
        sys.exit(1)

    # Create a mapping of old class IDs to new class IDs based on command line arguments
    class_mapping = {int(old): index for index, old in enumerate(sys.argv[1:])}

    # Search for all .txt files in the current directory and its subdirectories
    for root, _, files in os.walk('.'):
        for file in files:
            if file.endswith('.txt'):
                renumber_classes(os.path.join(root, file), class_mapping)

    print("Renumbering complete!")

if __name__ == "__main__":
    main()

