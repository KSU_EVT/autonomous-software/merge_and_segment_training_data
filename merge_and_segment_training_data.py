#!/usr/bin/env python3
"""
merge_and_segment_training_data.py

This script is intended to mix together Yolo-format labels and images from two separate datasets according to an arbitrary ratio of each defined by a rational number

The script will then divide the output randomly into an arbitrary whole number N of folders, each containing its own "images" and "labels" folder.

Using (whichever version of) YOLO (that we choose), a series of N training runs will be performed, each time rotating which folder is used as the evaluation set (while the N - 1 other folders are used as the training set, or N-2 if one folder is designated as the testing set). This can be done by editing the <trainingconfig>.yaml file between each run, and using the best.pt weights file output from one run as the input weights file for the next run

The technique described above should allow our model to be trained evenly using as much of our data as possible, while minimizing the risk of over-fitting with a small dataset

"""
import sys
import os
import random
import shutil
from fractions import Fraction
import math


def get_file_pairs(directory):
    all_files = [f for f in os.listdir(directory) if not f.startswith('.')]
    images = [f for f in all_files if f.endswith(('.jpg', '.jpeg', '.png', '.JPG', '.JPEG', '.PNG'))]
    labels = [f for f in all_files if f.endswith('.txt')]

    pairs = []
    for img in images:
        label = os.path.splitext(img)[0] + '.txt'
        if label in labels:
            pairs.append((img, label))
        else:
            print(f"Error: Image file '{img}' does not have a corresponding label in directory '{directory}'.")
            sys.exit(1)
    return pairs


def distribute_files(input_directory_A, input_directory_B, output_directory, num_segments, sample_ratio):
    pairs_a = get_file_pairs(input_directory_A)
    pairs_b = get_file_pairs(input_directory_B)

    total_pairs = len(pairs_a) + len(pairs_b)
    pairs_per_segment = total_pairs // num_segments

    # Calculate the number of pairs from A and B based on the specified ratio for each segment
    pairs_from_a = pairs_per_segment * sample_ratio.numerator // (sample_ratio.numerator + sample_ratio.denominator)

    # Shuffle the pairs to randomize their order
    random.shuffle(pairs_a)
    random.shuffle(pairs_b)

    for i in range(num_segments):
        segment_dir = os.path.join(output_directory, str(i))
        image_dir = os.path.join(segment_dir, 'images')
        label_dir = os.path.join(segment_dir, 'labels')
        os.makedirs(image_dir, exist_ok=True)
        os.makedirs(label_dir, exist_ok=True)

        # Get pairs for this segment from directory A
        for j in range(pairs_from_a):
            if pairs_a:
                img, label = pairs_a.pop()
                shutil.copy(os.path.join(input_directory_A, img), os.path.join(image_dir, img))
                shutil.copy(os.path.join(input_directory_A, label), os.path.join(label_dir, label))

        # Get the remaining pairs for this segment from directory B
        for j in range(pairs_per_segment - pairs_from_a):
            if pairs_b:
                img, label = pairs_b.pop()
                shutil.copy(os.path.join(input_directory_B, img), os.path.join(image_dir, img))
                shutil.copy(os.path.join(input_directory_B, label), os.path.join(label_dir, label))

def main():
    # Ensure the right number of arguments are given
    if len(sys.argv) != 6:
        print("Usage: script.py <input_directory_A> <input_directory_B> <output_directory> <num_segments> <ratio>")
        sys.exit(1)

    input_directory_A = sys.argv[1]
    input_directory_B = sys.argv[2]
    output_directory = sys.argv[3]
    num_segments = int(sys.argv[4])

    # Convert the ratio argument to a fraction
    try:
        x, y = map(int, sys.argv[5].split(':'))  # Splitting the string at ':'
        sample_ratio = Fraction(x, y)  # Constructing the fraction using the numerator and denominator
    except ValueError:
        print("Error: Ratio should be in the format X:Y where X and Y are integers.")
        sys.exit(1)

    # Validate directories exist
    if not os.path.exists(input_directory_A) or not os.path.exists(input_directory_B):
        print("Error: One of the input directories does not exist.")
        sys.exit(1)

    # Create the output directory if it doesn't exist
    if not os.path.exists(output_directory):
        os.makedirs(output_directory)

    distribute_files(input_directory_A, input_directory_B, output_directory, num_segments, sample_ratio)

if __name__ == "__main__":
    main()
