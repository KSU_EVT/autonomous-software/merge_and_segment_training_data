#!/usr/bin/env python3
def convert_lines_to_list_format(filename):
    with open(filename, 'r') as f:
        # Read lines and remove newline characters
        lines = f.read().splitlines()

    # Convert lines to desired format
    output = "[ '" + "', '".join(lines) + "' ]"

    return output

if __name__ == "__main__":
    filename = input("Enter the name of the file: ")
    result = convert_lines_to_list_format(filename)
    print(result)
